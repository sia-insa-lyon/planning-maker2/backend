from rest_framework import serializers

from planning.models import Volunteer
from authentication.models import User
from planning.serializers.AvailabilitySerializer import AvailabilitySerializer

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'phone', 'address', 'year']
        
class NameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name']
class VolunteerSerializer(serializers.ModelSerializer):
    user = UserSerializer()  # Sérialiseur imbriqué pour inclure les informations de l'utilisateur
    availability = AvailabilitySerializer(many=True, read_only=True, source='availability_set')

    class Meta:
        model = Volunteer
        fields = ['id', 'user', 'event', 'role', 'friends', 'availability', 'trust_level', 'tag', 'permission']  # Incluez les autres champs du modèle Volunteer
        
    def get_first_name(self, obj):
        return obj.user.first_name

    def get_last_name(self, obj):
        return obj.user.last_name
