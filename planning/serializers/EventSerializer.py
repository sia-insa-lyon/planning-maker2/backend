from rest_framework import serializers

from planning.models import Event
from planning.serializers.RoleSerializer import RoleSerializer
from planning.serializers.TagSerializer import TagSerializer


class EventSerializer(serializers.ModelSerializer):
    roles = RoleSerializer(many=True, read_only=True, source='role_set')
    tags = TagSerializer(many=True, read_only=True, source='tag_set')

    def validate(self, data):
        start_date = data.get('start_date')
        end_date = data.get('end_date')

        # Vérification de la validité des dates
        if start_date and end_date and start_date >= end_date:
            raise serializers.ValidationError('Start date must be before end date')

        start_registration_date = data.get('start_registration_date')
        end_registration_date = data.get('end_registration_date')

        # Verification de l'existance d'une période d'inscription
        if start_registration_date and end_registration_date and start_registration_date >= end_registration_date:
            raise serializers.ValidationError('Start registration date must be before end registration date')

        return data

    class Meta:
        model = Event
        fields = '__all__'
