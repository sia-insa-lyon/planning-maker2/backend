from rest_framework import serializers

from planning.models import MacroTask
from planning.serializers.AffectationSerializer import AffectationSerializer
from planning.serializers.TaskSerializer import TaskSerializer

class MacroTaskGetSerializer(serializers.ModelSerializer):
    affected_volunteers = AffectationSerializer(many=True, read_only=True, source='affectation_set')
    task = TaskSerializer()
    class Meta:
        model = MacroTask
        fields = '__all__'

class MacroTaskPutSerializer(serializers.ModelSerializer):
    class Meta:
        model = MacroTask
        fields = '__all__'
