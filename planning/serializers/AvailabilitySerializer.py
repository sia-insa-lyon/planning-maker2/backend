from rest_framework import serializers

from planning.models import Availability
from planning.serializers.TimeWindowSerializer import TimeWindowSerializer


class AvailabilitySerializer(serializers.ModelSerializer):
    time_window = TimeWindowSerializer()
    class Meta:
        model = Availability
        fields = '__all__'
