from rest_framework import serializers

from planning.models import TimeWindow

class TimeWindowSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeWindow
        fields = '__all__'
