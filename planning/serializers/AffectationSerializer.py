from rest_framework import serializers

from planning.models import Affectation
from planning.serializers.VolunteerSerializer import VolunteerSerializer


class AffectationSerializer(serializers.Serializer):
    macro_task = serializers.SerializerMethodField()
    volunteer = VolunteerSerializer()

    class Meta:
        model = Affectation
        fields = '__all__'

    def get_macro_task(self, obj):
        from planning.serializers.MacroTaskSerializer import MacroTaskGetSerializer
        serializer = MacroTaskGetSerializer(obj.macro_task)
        return serializer.data
