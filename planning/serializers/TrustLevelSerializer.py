from rest_framework import serializers

from planning.models import TrustLevel

class TrustLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrustLevel
        fields = '__all__'
