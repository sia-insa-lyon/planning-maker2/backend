from rest_framework import serializers

from planning.models import TrustLevelPerMacroTask

class TrustLevelPerMacroTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrustLevelPerMacroTask
        fields = '__all__'
