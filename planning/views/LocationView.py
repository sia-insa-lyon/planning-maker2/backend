from rest_framework import viewsets, status
from rest_framework.permissions import OR
from rest_framework.response import Response

from django.shortcuts import get_object_or_404

from planning.models import Location, Event
from planning.serializers.LocationSerializer import LocationSerializer
from planning.permissions import IsSuperAdmin, IsAdmin, IsManager, IsSuperManager


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    def get_permissions(self):
        event_id = self.kwargs.get('event_id')
        permission_classes = [IsManager(event_id)]
        return  permission_classes


    def list(self, request, event_id=None):
        queryset = self.get_queryset().filter(event=event_id)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    def create(self, request, *args, **kwargs):
        event_id = self.kwargs.get('event_id')
        event = get_object_or_404(Event, pk=event_id)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(event=event)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, pk=None, event_id=None):
        location = get_object_or_404(Location, id=pk, event_id=event_id)
        serializer = LocationSerializer(location)
        return Response(serializer.data)

    def update(self, request, pk=None, event_id=None):
        location = get_object_or_404(Location, id=pk, event_id=event_id)
        serializer = LocationSerializer(location, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def partial_update(self, request, pk=None, event_id=None):
        location = get_object_or_404(self.get_queryset(), pk=pk, event_id=event_id)
        serializer = self.serializer_class(location, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None, event_id=None):
        location = get_object_or_404(Location, id=pk, event_id=event_id)
        location.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
