from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import status, viewsets

from planning.models import TrustLevel

from planning.serializers.TrustLevelSerializer import TrustLevelSerializer


class TrustLevelViewSet(viewsets.ModelViewSet):
    queryset = TrustLevel.objects.all()
    serializer_class = TrustLevelSerializer
