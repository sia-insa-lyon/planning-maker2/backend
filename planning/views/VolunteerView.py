from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from planning.models import Volunteer, Availability, TimeWindow, Affectation, Friendship
from planning.permissions import IsSuperAdmin, IsAdmin
from planning.serializers.MacroTaskSerializer import MacroTaskPutSerializer
from planning.serializers.TimeWindowSerializer import TimeWindowSerializer
from planning.serializers.VolunteerSerializer import VolunteerSerializer, NameSerializer


class VolunteerViewSet(viewsets.ModelViewSet):
    serializer_class = VolunteerSerializer

    def get_queryset(self):
        event_id = self.kwargs.get('event_id')
        return Volunteer.objects.filter(event=event_id)

    def get_permissions(self):
        if self.action in ['list', 'create', 'retrieve']:
            permission_classes = [IsAuthenticated()]
        elif self.action in ['update', 'partial_update', 'destroy']:
            event_id = self.kwargs.get('event_id')
            permission_classes = [IsAdmin(event_id)]
        else:
            permission_classes = [IsSuperAdmin()]

        # todo permissions for delete_availability
        return permission_classes

    def list(self, request, *args, **kwargs):
        event_id = self.kwargs.get('event_id')
        queryset = self.get_queryset().filter(event=event_id)
        if IsAdmin(event_id).has_permission(request, self) or IsSuperAdmin().has_permission(request, self):
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        else:
            serializer = NameSerializer(queryset, many=True)
            return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        return Response({'error': 'Method "POST" not allowed.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def retrieve(self, request, pk=None, event_id=None):
        volunteer = get_object_or_404(Volunteer, user_id=pk, event_id=event_id)
        event_id = self.kwargs.get('event_id')
        if IsAdmin(event_id).has_permission(request, self) or IsSuperAdmin().has_permission(request, self):
            serializer = self.get_serializer(volunteer)
            return Response(serializer.data)
        else:
            serializer = NameSerializer(volunteer)
            return Response(serializer.data)

    def update(self, request, pk=None, event_id=None):
        volunteer = get_object_or_404(Volunteer, user_id=pk, event_id=event_id)
        serializer = VolunteerSerializer(volunteer, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None, event_id=None):
        volunteer = get_object_or_404(self.get_queryset(), pk=pk, event_id=event_id)
        serializer = self.serializer_class(volunteer, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None, event_id=None):
        try:
            volunteer_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=volunteer_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        volunteer.delete()

    @action(detail=True, methods=['get'])
    def availabilities(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        time_windows = []

        time_window_ids = Availability.objects.filter(volunteer=volunteer).values_list('time_window', flat=True)

        for time_window_id in time_window_ids:
            try:
                time_window = TimeWindow.objects.get(id=time_window_id)
            except TimeWindow.DoesNotExist:
                return Response({'error': 'TimeWindow {} does not exist'.format(time_window_id)},
                                status=status.HTTP_400_BAD_REQUEST)

            time_windows.append(time_window)

        serializer = TimeWindowSerializer(time_windows, many=True)
        response_data = {
            'volunteer_id': volunteer.id,
            'availabilities': serializer.data
        }

        return Response(response_data)

    @action(detail=True, methods=['post'])
    @transaction.atomic
    def add_availabilities(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        availabilities = request.data.get('availabilities', [])

        time_windows = []

        for availability in availabilities:
            try:
                time_window = TimeWindow.objects.get(id=availability)
            except TimeWindow.DoesNotExist:
                return Response({'error': 'TimeWindow {} does not exist'.format(availability)},
                                status=status.HTTP_400_BAD_REQUEST)

            # Check if volunteer has already this availability
            if Availability.objects.filter(volunteer=volunteer, time_window=time_window).exists():
                return Response({'error': 'Volunteer {} already has this availability'.format(volunteer.id)},
                                status=status.HTTP_400_BAD_REQUEST)

            # Check if the time_window is in the right event
            if time_window.event != volunteer.event:
                return Response({'error': 'TimeWindow {} is not in the right event'.format(availability)},
                                status=status.HTTP_400_BAD_REQUEST)

            time_windows.append(time_window)

        for time_window in time_windows:
            Availability.objects.create(volunteer=volunteer, time_window=time_window)

        serializer = TimeWindowSerializer(time_windows, many=True)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['delete'])
    @transaction.atomic
    def remove_availabilities(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        availabilities = request.query_params.get('availabilities', '').split(',')

        time_windows = []

        for availability in availabilities:
            try:
                time_window = TimeWindow.objects.get(id=availability)
            except TimeWindow.DoesNotExist:
                return Response({'error': 'TimeWindow {} does not exist'.format(availability)},
                                status=status.HTTP_400_BAD_REQUEST)

            # Check if volunteer has already this availability
            if not Availability.objects.filter(volunteer=volunteer, time_window=time_window).exists():
                return Response({'error': 'Volunteer {} does not have this availability'.format(volunteer.id)},
                                status=status.HTTP_400_BAD_REQUEST)

            time_windows.append(time_window)

        for time_window in time_windows:
            Availability.objects.filter(volunteer=volunteer, time_window=time_window).delete()

        # Delete all affections for this volunteer
        affectations = Affectation.objects.filter(volunteer=volunteer)

        for affectation in affectations:
            macro_task = affectation.macro_task

            for time_window in time_windows:
                if macro_task.start_date <= time_window.start_date <= macro_task.end_date \
                        or macro_task.start_date <= time_window.end_date <= macro_task.end_date:
                    affectation.delete()

        serializer = TimeWindowSerializer(time_windows, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def tasks(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        tasks = []

        affectations = Affectation.objects.filter(volunteer=volunteer)

        for affectation in affectations:
            tasks.append(affectation.macro_task)

        serializer = MacroTaskPutSerializer(tasks, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def friends(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        friends = []

        friendships = Friendship.objects.filter(volunteer=volunteer)

        for friendship in friendships:
            friends.append(friendship.friend)

        serializer = VolunteerSerializer(friends, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    @transaction.atomic
    def add_friends(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        friends = []

        for friend in request.data.get('friends', []):
            try:
                friend = Volunteer.objects.get(id=friend)
            except Volunteer.DoesNotExist:
                return Response({'error': 'Volunteer {} does not exist'.format(friend)},
                                status=status.HTTP_400_BAD_REQUEST)

            if friend.event != volunteer.event:
                return Response({'error': 'Volunteer {} is not in the right event'.format(friend)},
                                status=status.HTTP_400_BAD_REQUEST)

            if Friendship.objects.filter(volunteer=volunteer, friend=friend).exists():
                return Response({'error': 'Volunteer {} is already friend with {}'.format(volunteer.id, friend.id)},
                                status=status.HTTP_400_BAD_REQUEST)

            friends.append(friend)

        for friend in friends:
            Friendship.objects.create(volunteer=volunteer, friend=friend)

        serializer = VolunteerSerializer(friends, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['delete'])
    @transaction.atomic
    def remove_friends(self, request, pk=None, event_id=None):
        try:
            user_id = int(pk)
            volunteer = Volunteer.objects.get(user_id=user_id, event_id=event_id)
        except Volunteer.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        friends = []

        for friend in request.query_params.get('friends', '').split(','):
            try:
                friend = Volunteer.objects.get(id=friend)
            except Volunteer.DoesNotExist:
                return Response({'error': 'Volunteer {} does not exist'.format(friend)},
                                status=status.HTTP_400_BAD_REQUEST)

            if not Friendship.objects.filter(volunteer=volunteer, friend=friend).exists():
                return Response({'error': 'Volunteer {} is not friend with {}'.format(volunteer.id, friend.id)},
                                status=status.HTTP_400_BAD_REQUEST)

            friends.append(friend)

        for friend in friends:
            Friendship.objects.filter(volunteer=volunteer, friend=friend).delete()

        serializer = VolunteerSerializer(friends, many=True)

        return Response(serializer.data)
