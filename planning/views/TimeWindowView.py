from rest_framework import viewsets, status
from rest_framework.response import Response

from django.shortcuts import get_object_or_404

from planning.models import TimeWindow, Event
from planning.serializers.TimeWindowSerializer import TimeWindowSerializer
from planning.permissions import IsAdmin


class TimeWindowViewSet(viewsets.ModelViewSet):
    queryset = TimeWindow.objects.all()
    serializer_class = TimeWindowSerializer

    def get_permissions(self):
        event_id = self.kwargs.get('event_id')
        permission_classes = [IsAdmin(event_id)]
        return permission_classes


    def list(self, request, event_id=None):
        queryset = self.get_queryset().filter(event=event_id).order_by('start_date')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    def create(self, request, *args, **kwargs):
        event_id = self.kwargs.get('event_id')
        event = get_object_or_404(Event, pk=event_id)
        serializer = self.serializer_class(data=request.data)
        print(request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(event=event)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    def retrieve(self, request, pk=None, event_id=None):
        timeWindow = get_object_or_404(TimeWindow, id=pk, event_id=event_id)
        serializer = self.serializer_class(timeWindow)
        return Response(serializer.data)


    def update(self, request, pk=None, event_id=None):
        timeWindow = get_object_or_404(TimeWindow, id=pk, event_id=event_id)
        serializer = self.serializer_class(timeWindow, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def partial_update(self, request, pk=None, event_id=None):
        timeWindow = get_object_or_404(self.get_queryset(), pk=pk, event_id=event_id)
        serializer = self.serializer_class(timeWindow, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, pk=None, event_id=None):
        timeWindow = get_object_or_404(TimeWindow, id=pk, event_id=event_id)
        timeWindow.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
