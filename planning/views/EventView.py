from django.db import transaction
from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated, OR
from rest_framework.response import Response

from authentication.models import User
from planning.models import Event, Volunteer
from planning.permissions import IsSuperAdmin, IsAdmin
from planning.serializers.EventSerializer import EventSerializer
from planning.serializers.TimeWindowSerializer import TimeWindowSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [IsAuthenticated()]
        elif self.action in ['update', 'partial_update']:
            event_id = self.kwargs.get('pk')
            permission_classes = [IsAdmin(event_id)]
        elif self.action in ['available_volunteers']:
            event_id = self.kwargs.get('pk')
            permission_classes = [IsAdmin(event_id)]
        else:
            permission_classes = [IsSuperAdmin()]
        return permission_classes

    def list(self, request, **kwargs):
        if request.user.is_superuser:
            queryset = self.get_queryset()
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        else:
            events = Volunteer.objects.filter(user=request.user).values('event')
            queryset = self.get_queryset().filter(id__in=events)
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

    @transaction.atomic
    def create(self, request, **kwargs):
        data = request.data
        data['creator'] = request.user.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)
