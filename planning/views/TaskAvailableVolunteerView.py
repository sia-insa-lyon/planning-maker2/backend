from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from planning.models import Affectation, MacroTask, Volunteer, TimeWindow, Availability
from planning.serializers.VolunteerSerializer import VolunteerSerializer


class TaskAvailableVolunteerView(APIView):
    def get(self, request, pk):
        macro_task_id = int(pk)

        try:
            macro_task = MacroTask.objects.get(id=macro_task_id)
        except MacroTask.DoesNotExist:
            return Response({'error': 'MacroTask {} does not exist'.format(macro_task_id)},
                            status=status.HTTP_400_BAD_REQUEST)

        volunteers = []

        for volunteer in Volunteer.objects.filter(event=macro_task.task.event):
            available = True

            # Check if volunteer is affected to a macro_task at the same time
            for affectation in Affectation.objects.filter(volunteer=volunteer):
                if affectation.macro_task.start_date <= macro_task.start_date <= affectation.macro_task.end_date \
                        or affectation.macro_task.start_date <= macro_task.end_date <= affectation.macro_task.end_date:
                    available = False

            # Check if volunteer is available
            time_windows = TimeWindow.objects.filter(event=macro_task.task.event)

            filtered_time_windows = []

            for time_window in time_windows:
                if time_window.intersects(macro_task):
                    filtered_time_windows.append(time_window)

            availability = Availability.objects.filter(volunteer=volunteer)

            for time_window in filtered_time_windows:
                if not availability.filter(time_window=time_window).exists():
                    available = False

            if available:
                volunteers.append(volunteer)

        serializer = VolunteerSerializer(volunteers, many=True)

        return Response(serializer.data)
