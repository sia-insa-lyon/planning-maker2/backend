from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import status, viewsets

from planning.models import TrustLevelPerMacroTask

from planning.serializers.TrustLevelPerMacroTaskSerializer import TrustLevelPerMacroTaskSerializer


class TrustLevelPerMacroTaskViewSet(viewsets.ModelViewSet):
    queryset = TrustLevelPerMacroTask.objects.all()
    serializer_class = TrustLevelPerMacroTaskSerializer
