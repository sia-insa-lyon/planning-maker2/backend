from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from django.shortcuts import get_object_or_404

from planning.models import Task, Event, Volunteer
from planning.serializers.TaskSerializer import TaskSerializer
from planning.serializers.MacroTaskSerializer import MacroTaskPutSerializer
from planning.permissions import IsMember, IsManager


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_permissions(self):
        event_id = self.kwargs.get('event_id')
        permission_classes = [IsMember(event_id)]
        return  permission_classes

    def list(self, request, event_id = None):
        event_id = self.kwargs.get('event_id')
        tasks = Task.objects.filter(event=event_id)
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data)
    

    def create(self, request, *args, **kwargs):
        event_id = self.kwargs.get('event_id')
        event = get_object_or_404(Event, pk=event_id)
        volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
        request.data['creator'] = volunteer.id
        #Vérifie la permission du user
        if IsManager(event_id).has_permission(request, self):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save(event=event)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else: #Cas membre (hard)
            tag = request.data.get('tag')
            #récupérer le volontaire associé à l'user de la requete
            volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
            volunteer_tags = volunteer.tag.all()
            volunteer_tags = volunteer_tags.values_list('id', flat=True)            
            if tag in volunteer_tags:
                serializer = self.get_serializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save(event=event)
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
            else:
                return Response({'error': 'Method "POST" not allowed for this tag.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
               
    
    def retrieve(self, request, pk=None, event_id=None):
        task = get_object_or_404(Task, id=pk, event_id=event_id)
        serializer = self.get_serializer(task)
        return Response(serializer.data)


    def update(self, request, pk=None, event_id=None):
        task = get_object_or_404(Task, id=pk, event_id=event_id)
        serializer = TaskSerializer(task, data=request.data)
        if serializer.is_valid():
            if IsManager(event_id).has_permission(request, self):
                serializer.save()
                return Response(serializer.data)
            else:
                tag = task.tag_id
                #récupérer le volontaire associé à l'user de la requete
                volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
                volunteer_tags = volunteer.tag.all()
                volunteer_tags = volunteer_tags.values_list('id', flat=True)            
                if tag in volunteer_tags:
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response({'error': 'Method "POST" not allowed for this tag.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def partial_update(self, request, pk=None, event_id=None):
        task = get_object_or_404(self.get_queryset(), pk=pk, event_id=event_id)
        serializer = self.serializer_class(task, data=request.data, partial=True)
        if serializer.is_valid():
            if IsManager(event_id).has_permission(request, self):
                serializer.save()
                return Response(serializer.data)
            else:
                tag = task.tag_id
                #récupérer le volontaire associé à l'user de la requete
                volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
                volunteer_tags = volunteer.tag.all()
                volunteer_tags = volunteer_tags.values_list('id', flat=True)            
                if tag in volunteer_tags:
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response({'error': 'Method "POST" not allowed for this tag.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None, event_id=None):
        task = get_object_or_404(Task, id=pk, event_id=event_id)
        if IsManager(event_id).has_permission(request, self):
            task.delete()
        else:
            tag = task.tag_id
            #récupérer le volontaire associé à l'user de la requete
            volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
            volunteer_tags = volunteer.tag.all()
            volunteer_tags = volunteer_tags.values_list('id', flat=True)            
            if tag in volunteer_tags:
                task.delete()
            else:
                return Response({'error': 'Method "POST" not allowed for this tag.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['post'])
    def create_macro_task(self, request, pk=None, event_id=None):
        event_id = self.kwargs.get('event_id')
        event = get_object_or_404(Event, pk=event_id)


        request.data['task']=pk
        # Vérifie la permission du user

        if IsManager(event_id).has_permission(request, self):
            serializer = MacroTaskPutSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:  # Cas membre (hard)
            task = request.data.get('task')
            task = get_object_or_404(Task, id=task, event_id=event_id)
            tag = task.tag_id
            # récupérer le volontaire associé à l'user de la requete
            volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
            volunteer_tags = volunteer.tag.all()
            volunteer_tags = volunteer_tags.values_list('id', flat=True)
            if tag in volunteer_tags:
                serializer = self.get_serializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save(event=event)
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
            else:
                return Response({'error': 'Method "POST" not allowed for this tag.'},
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)

