from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from planning.models import MacroTask, Volunteer, TimeWindow, Affectation, Availability
from planning.permissions import IsMember, IsAdmin, IsManager
from planning.serializers.MacroTaskSerializer import MacroTaskGetSerializer, MacroTaskPutSerializer
from planning.serializers.VolunteerSerializer import VolunteerSerializer


class MacroTaskViewSet(viewsets.ModelViewSet):
    queryset = MacroTask.objects.all()
    serializer_class = MacroTaskGetSerializer

    def get_permissions(self):
        event_id = self.kwargs.get('event_id')
        if self.action == 'list':
            permission_classes = [IsMember(event_id)]
        elif self.action in ['available_volunteers']:
            permission_classes = [IsAdmin(event_id)]
        else:
            permission_classes = []

        return permission_classes

    def list(self, request, event_id=None):
        event_id = self.kwargs.get('event_id')
        macro_tasks = MacroTask.objects.filter(task__event_id=event_id)
        serializer = MacroTaskGetSerializer(macro_tasks, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        return Response({'error': 'Method "POST" not allowed.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def retrieve(self, request, pk=None, event_id=None):
        macro_task = get_object_or_404(MacroTask, id=pk, event_id=event_id)
        serializer = MacroTaskGetSerializer(macro_task)
        return Response(serializer.data)

    def update(self, request, pk=None, event_id=None):
        macro_task = get_object_or_404(MacroTask, id=pk, task__event_id=event_id)
        serializer = MacroTaskGetSerializer(macro_task, data=request.data)
        if serializer.is_valid():
            if IsManager(event_id).has_permission(request, self):
                serializer.save()
                return Response(serializer.data)
            else:
                tag = macro_task.task__tag_id
                # récupérer le volontaire associé à l'user de la requete
                volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
                volunteer_tags = volunteer.tag.all()
                volunteer_tags = volunteer_tags.values_list('id', flat=True)
                if tag in volunteer_tags:
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response({'error': 'Method "POST" not allowed for this tag.'},
                                    status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None, event_id=None):
        macro_task = get_object_or_404(self.get_queryset(), pk=pk, task__event_id=event_id)
        serializer = self.serializer_class(macro_task, data=request.data, partial=True)
        if serializer.is_valid():
            if IsManager(event_id).has_permission(request, self):
                serializer.save()
                return Response(serializer.data)
            else:
                tag = macro_task.task__tag_id
                # récupérer le volontaire associé à l'user de la requete
                volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
                volunteer_tags = volunteer.tag.all()
                volunteer_tags = volunteer_tags.values_list('id', flat=True)
                if tag in volunteer_tags:
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response({'error': 'Method "POST" not allowed for this tag.'},
                                    status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None, event_id=None):
        macro_task = get_object_or_404(MacroTask, id=pk, task__event_id=event_id)
        if IsManager(event_id).has_permission(request, self):
            macro_task.delete()
        else:
            tag = macro_task.task__tag_id
            # récupérer le volontaire associé à l'user de la requete
            volunteer = get_object_or_404(Volunteer, user_id=request.user.id, event_id=event_id)
            volunteer_tags = volunteer.tag.all()
            volunteer_tags = volunteer_tags.values_list('id', flat=True)
            if tag in volunteer_tags:
                macro_task.delete()
            else:
                return Response({'error': 'Method "POST" not allowed for this tag.'},
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['get'])
    def available_volunteers(self, request, pk=None, event_id=None):
        try:
            macro_task_id = int(pk)
            macro_task = MacroTask.objects.get(id=macro_task_id)
        except MacroTask.DoesNotExist:
            return Response({'error': 'MacroTask {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        volunteers = []

        time_windows = TimeWindow.objects.filter(event=macro_task.task.event)

        filtered_time_windows = []

        for time_window in time_windows:
            if time_window.intersects(macro_task):
                filtered_time_windows.append(time_window)

        for volunteer in Volunteer.objects.filter(event=macro_task.task.event):
            available = True

            # Check if volunteer is affected to a macro_task at the same time
            for affectation in Affectation.objects.filter(volunteer=volunteer):
                if affectation.macro_task.start_date <= macro_task.start_date <= affectation.macro_task.end_date \
                        or affectation.macro_task.start_date <= macro_task.end_date <= affectation.macro_task.end_date:
                    available = False

            # Check if volunteer is available
            availability = Availability.objects.filter(volunteer=volunteer)

            for time_window in filtered_time_windows:
                if not availability.filter(time_window=time_window).exists():
                    available = False

            if available:
                volunteers.append(volunteer)

        serializer = VolunteerSerializer(volunteers, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    @transaction.atomic
    def affect_volunteer(self, request, pk=None, event_id=None):
        try:
            macro_task_id = int(pk)
            macro_task = MacroTask.objects.get(id=macro_task_id)
        except MacroTask.DoesNotExist:
            return Response({'error': 'MacroTask {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        volunteers = []

        time_windows = TimeWindow.objects.filter(event=macro_task.task.event)

        filtered_time_windows = []

        for time_window in time_windows:
            if time_window.intersects(macro_task):
                filtered_time_windows.append(time_window)

        for volunteer_id in request.data.get('volunteers', []):
            try:
                volunteer = Volunteer.objects.get(id=volunteer_id)
            except Volunteer.DoesNotExist:
                return Response({'error': 'Volunteer {} does not exist'.format(volunteer_id)},
                                status=status.HTTP_400_BAD_REQUEST)

            if Affectation.objects.filter(volunteer_id=volunteer_id, macro_task_id=macro_task_id).exists():
                return Response({'error': 'Volunteer {} is already assigned to macro task {}'.format(
                    volunteer_id, macro_task_id
                )}, status=status.HTTP_400_BAD_REQUEST)

            count = macro_task.count_orgas

            if Affectation.objects.filter(macro_task_id=macro_task_id).count() >= count:
                return Response({'error': 'MacroTask {} is full'.format(macro_task_id)},
                                status=status.HTTP_400_BAD_REQUEST)

            # Check if volunteer is affected to a macro_task at the same time
            for affectation in Affectation.objects.filter(volunteer_id=volunteer_id):
                if affectation.macro_task.start_date <= macro_task.start_date <= affectation.macro_task.end_date \
                        or affectation.macro_task.start_date <= macro_task.end_date <= affectation.macro_task.end_date:
                    return Response(
                        {'error': 'Volunteer {} is already assigned to a macro task at the same time'.format(
                            volunteer_id
                        )}, status=status.HTTP_400_BAD_REQUEST)

            # Check if volunteer is available
            availability = Availability.objects.filter(volunteer=volunteer, time_window__in=time_windows)

            for time_window in filtered_time_windows:
                if not availability.filter(time_window=time_window).exists():
                    return Response({'error': 'Volunteer {} is not available'.format(volunteer_id)},
                                    status=status.HTTP_400_BAD_REQUEST)

            volunteers.append(volunteer)

        for volunteer in volunteers:
            Affectation.objects.create(volunteer=volunteer, macro_task=macro_task)

        serializer = MacroTaskPutSerializer(macro_task)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['get'])
    def affected_volunteers(self, request, pk=None, event_id=None):
        try:
            macro_task_id = int(pk)
            macro_task = MacroTask.objects.get(id=macro_task_id)
        except MacroTask.DoesNotExist:
            return Response({'error': 'Volunteer {} does not exist'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

        volunteers = []

        volunteer_ids = Affectation.objects.filter(macro_task=macro_task).values_list('volunteer', flat=True)

        for volunteer_id in volunteer_ids:
            try:
                volunteer = Volunteer.objects.get(id=volunteer_id)
            except Volunteer.DoesNotExist:
                return Response({'error': 'Volunteer {} does not exist'.format(volunteer_id)},
                                status=status.HTTP_400_BAD_REQUEST)

            volunteers.append(volunteer)

        serializer = VolunteerSerializer(volunteers, many=True)

        return Response(serializer.data)
