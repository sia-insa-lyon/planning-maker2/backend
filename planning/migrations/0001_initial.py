# Generated by Django 4.2 on 2023-05-12 06:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True, max_length=10000, null=True)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('start_registration_date', models.DateTimeField()),
                ('end_registration_date', models.DateTimeField()),
                ('trust_level_count', models.IntegerField(default=5)),
                ('creator', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Event',
                'verbose_name_plural': 'Events',
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
                ('longitude', models.FloatField(blank=True, null=True)),
                ('latitude', models.FloatField(blank=True, null=True)),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'Location',
                'verbose_name_plural': 'Locations',
            },
        ),
        migrations.CreateModel(
            name='MacroTask',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('effective_start_date', models.DateTimeField(blank=True, null=True)),
                ('effective_end_date', models.DateTimeField(blank=True, null=True)),
                ('count_orgas', models.IntegerField()),
            ],
            options={
                'verbose_name': 'MacroTask',
                'verbose_name_plural': 'MacroTasks',
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('color', models.CharField(max_length=200)),
                ('trust_level', models.IntegerField(default=0)),
                ('permission', models.CharField(choices=[('GUEST', 'Guest'), ('MEMBER', 'Member'), ('MANAGER', 'Manager'), ('SUPER_MANAGER', 'Super Manager'), ('ADMIN', 'Admin'), ('SUPER_ADMIN', 'Super Admin')], default='GUEST', max_length=30, verbose_name='permission')),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'Role',
                'verbose_name_plural': 'Roles',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('color', models.CharField(max_length=200)),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.CreateModel(
            name='TrustLevel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('trust_level', models.IntegerField(default=0)),
                ('count_volunteers', models.PositiveIntegerField(default=0)),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'TrustLevel',
                'verbose_name_plural': 'TrustLevels',
            },
        ),
        migrations.CreateModel(
            name='Volunteer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('trust_level', models.IntegerField(default=0)),
                ('permission', models.CharField(choices=[('GUEST', 'Guest'), ('MEMBER', 'Member'), ('MANAGER', 'Manager'), ('SUPER_MANAGER', 'Super Manager'), ('ADMIN', 'Admin'), ('SUPER_ADMIN', 'Super Admin')], default='GUEST', max_length=30, verbose_name='permission')),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
                ('role', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='planning.role')),
                ('tag', models.ManyToManyField(blank=True, to='planning.tag')),
                ('user', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Volunteer',
                'verbose_name_plural': 'Volunteers',
            },
        ),
        migrations.CreateModel(
            name='TrustLevelPerMacroTask',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count_volunteers', models.PositiveIntegerField()),
                ('macro_task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='planning.macrotask')),
                ('trust_level', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='planning.trustlevel')),
            ],
            options={
                'verbose_name': 'TrustLevelPerMacroTask',
                'verbose_name_plural': 'TrustLevelsPerMacroTask',
            },
        ),
        migrations.CreateModel(
            name='TimeWindow',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('value', models.IntegerField(default=0)),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'TimeWindow',
                'verbose_name_plural': 'TimeWindows',
            },
        ),
        migrations.CreateModel(
            name='TaskType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('color', models.CharField(max_length=200)),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
            ],
            options={
                'verbose_name': 'TaskType',
                'verbose_name_plural': 'TaskTypes',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True, max_length=10000, null=True)),
                ('checked', models.BooleanField(default=False)),
                ('hardship', models.IntegerField(default=0)),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='planning.volunteer')),
                ('end_location', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='end_location', to='planning.location')),
                ('event', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.event')),
                ('start_location', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='start_location', to='planning.location')),
                ('tag', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='planning.tag')),
                ('type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='planning.tasktype')),
            ],
            options={
                'verbose_name': 'Task',
                'verbose_name_plural': 'Tasks',
            },
        ),
        migrations.AddField(
            model_name='macrotask',
            name='manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='planning.volunteer'),
        ),
        migrations.AddField(
            model_name='macrotask',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='planning.task'),
        ),
        migrations.AddField(
            model_name='macrotask',
            name='trust_levels',
            field=models.ManyToManyField(through='planning.TrustLevelPerMacroTask', to='planning.trustlevel'),
        ),
        migrations.CreateModel(
            name='Friendship',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('friend', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='friends_of', to='planning.volunteer')),
                ('volunteer', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='friends', to='planning.volunteer')),
            ],
            options={
                'verbose_name': 'Friendship',
                'verbose_name_plural': 'Friendships',
            },
        ),
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_window', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.timewindow')),
                ('volunteer', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.volunteer')),
            ],
            options={
                'verbose_name': 'Availability',
                'verbose_name_plural': 'Availabilities',
            },
        ),
        migrations.CreateModel(
            name='Affectation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('macro_task', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.macrotask')),
                ('volunteer', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='planning.volunteer')),
            ],
            options={
                'verbose_name': 'Affectation',
                'verbose_name_plural': 'Affectations',
            },
        ),
    ]
