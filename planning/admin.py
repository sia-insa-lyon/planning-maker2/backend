from django.contrib import admin

from planning.models import (
    Availability,
    Event,
    Location,
    MacroTask,
    Affectation,
    Role,
    Tag,
    Task,
    TaskType,
    TimeWindow,
    Volunteer,
    TrustLevel,
    TrustLevelPerMacroTask
)


class EventAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


class VolunteerAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


class MacroTaskAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


admin.site.register(Availability)
admin.site.register(Event, EventAdmin)
admin.site.register(Location)
admin.site.register(MacroTask, MacroTaskAdmin)
admin.site.register(Affectation)
admin.site.register(Role)
admin.site.register(Tag)
admin.site.register(Task)
admin.site.register(TaskType)
admin.site.register(TimeWindow)
admin.site.register(Volunteer, VolunteerAdmin)
admin.site.register(TrustLevel)
admin.site.register(TrustLevelPerMacroTask)
