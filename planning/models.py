from django.db import models

GUEST = 'GUEST'
MEMBER = 'MEMBER'
MANAGER = 'MANAGER'
SUPER_MANAGER = 'SUPER_MANAGER'
ADMIN = 'ADMIN'
SUPER_ADMIN = 'SUPER_ADMIN'

PERMISSION_CHOICES = (
    (GUEST, 'Guest'),
    (MEMBER, 'Member'),
    (MANAGER, 'Manager'),
    (SUPER_MANAGER, 'Super Manager'),
    (ADMIN, 'Admin'),
    (SUPER_ADMIN, 'Super Admin'),
)


class Event(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=10000, blank=True, null=True)
    start_date = models.DateTimeField(null = False)
    end_date = models.DateTimeField(null = False)
    start_registration_date = models.DateTimeField()
    end_registration_date = models.DateTimeField()
    creator = models.ForeignKey('authentication.User', on_delete=models.CASCADE, blank=True, null=False)
    trust_level_count = models.IntegerField(default=5)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Event'
        verbose_name_plural = 'Events'


class Volunteer(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    user = models.ForeignKey('authentication.User', on_delete=models.CASCADE, blank=True, null=False)
    role = models.ForeignKey('Role', on_delete=models.SET_NULL, blank=True, null=True)
    trust_level = models.IntegerField(default=0)
    tag = models.ManyToManyField('Tag', blank=True)
    permission = models.CharField(max_length=30, choices=PERMISSION_CHOICES, verbose_name='permission', default=GUEST)

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        verbose_name = 'Volunteer'
        verbose_name_plural = 'Volunteers'


class Friendship(models.Model):
    volunteer = models.ForeignKey('Volunteer', on_delete=models.CASCADE, blank=True, null=False, related_name="friends")
    friend = models.ForeignKey('Volunteer', on_delete=models.CASCADE, blank=True, null=False, related_name="friends_of")

    def __str__(self):
        return self.volunteer.user.first_name + ' ' + self.volunteer.user.last_name + ' - ' \
            + self.friend.user.first_name + ' ' + self.friend.user.last_name

    class Meta:
        verbose_name = 'Friendship'
        verbose_name_plural = 'Friendships'


class Availability(models.Model):
    volunteer = models.ForeignKey('Volunteer', on_delete=models.CASCADE, blank=True, null=False)
    time_window = models.ForeignKey('TimeWindow', on_delete=models.CASCADE, blank=True, null=False)

    def __str__(self):
        return self.volunteer.user.first_name + ' ' + self.volunteer.user.last_name + ' - ' \
            + str(self.time_window.start_date) + ' - ' + str(self.time_window.end_date)

    class Meta:
        verbose_name = 'Availability'
        verbose_name_plural = 'Availabilities'


class TimeWindow(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    value = models.IntegerField(default=0)  # Points de charisme

    def __str__(self):
        return '[' + str(self.event) + ']' + ' ' + str(self.start_date) + ' - ' + str(self.end_date)

    def intersects(self, macro_task):
        if self.end_date == macro_task.start_date or self.start_date == macro_task.end_date:
            return False

        return macro_task.end_date >= self.start_date >= macro_task.start_date \
            or macro_task.start_date <= self.end_date <= macro_task.end_date

    class Meta:
        verbose_name = 'TimeWindow'
        verbose_name_plural = 'TimeWindows'


class Task(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=10000, blank=True, null=True)
    checked = models.BooleanField(default=False, blank=False, null=False)
    start_location = models.ForeignKey('Location', on_delete=models.PROTECT, null=False, related_name='start_location')
    end_location = models.ForeignKey('Location', on_delete=models.PROTECT, blank=True, null=True,
                                     related_name='end_location')
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    creator = models.ForeignKey('Volunteer', on_delete=models.SET_NULL, blank=True, null=True)
    type = models.ForeignKey('TaskType', on_delete=models.PROTECT, blank=True, null=True)
    tag = models.ForeignKey('Tag', on_delete=models.PROTECT, blank=True, null=True)
    hardship = models.IntegerField(default=0)  # Points Sheitan

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'


class MacroTask(models.Model):
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    effective_start_date = models.DateTimeField(blank=True, null=True)
    effective_end_date = models.DateTimeField(blank=True, null=True)
    count_orgas = models.IntegerField()  # Nombre orgas total
    trust_levels = models.ManyToManyField('TrustLevel', through='TrustLevelPerMacroTask')
    manager = models.ForeignKey('Volunteer', on_delete=models.PROTECT, blank=True, null=True)
    task = models.ForeignKey('Task', on_delete=models.CASCADE, null=False)

    def __str__(self):
        return self.task.title

    class Meta:
        verbose_name = 'MacroTask'
        verbose_name_plural = 'MacroTasks'


class Affectation(models.Model):
    volunteer = models.ForeignKey('Volunteer', on_delete=models.CASCADE, blank=True, null=False)
    macro_task = models.ForeignKey('MacroTask', on_delete=models.CASCADE, blank=True, null=False)

    def __str__(self):
        return self.volunteer.user.first_name + ' ' + self.volunteer.user.last_name + ' - ' + self.macro_task.task.title

    class Meta:
        verbose_name = 'Affectation'
        verbose_name_plural = 'Affectations'


class Location(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Location'
        verbose_name_plural = 'Locations'


class TaskType(models.Model):
    name = models.CharField(max_length=200)
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    color = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'TaskType'
        verbose_name_plural = 'TaskTypes'


class Tag(models.Model):
    name = models.CharField(max_length=200)
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    color = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'


class Role(models.Model):
    name = models.CharField(max_length=200)
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    color = models.CharField(max_length=200)
    trust_level = models.IntegerField(default=0)
    permission = models.CharField(max_length=30, choices=PERMISSION_CHOICES, verbose_name='permission', default=GUEST)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Role'
        verbose_name_plural = 'Roles'


class TrustLevel(models.Model):
    name = models.CharField(max_length=200)
    event = models.ForeignKey('Event', on_delete=models.CASCADE, blank=True, null=False)
    trust_level = models.IntegerField(default=0)
    count_volunteers = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'TrustLevel'
        verbose_name_plural = 'TrustLevels'


class TrustLevelPerMacroTask(models.Model):
    macro_task = models.ForeignKey('MacroTask', on_delete=models.CASCADE)
    trust_level = models.ForeignKey('TrustLevel', on_delete=models.CASCADE)
    count_volunteers = models.PositiveIntegerField()

    def __str__(self):
        return self.macro_task.title + ' - ' + self.trust_level.name

    class Meta:
        verbose_name = 'TrustLevelPerMacroTask'
        verbose_name_plural = 'TrustLevelsPerMacroTask'
