from rest_framework.permissions import BasePermission

from planning.models import Volunteer


def findVolunteer(user, event_id):
    return Volunteer.objects.filter(user=user.id,event=event_id).first()

class ReadOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        # Autorise uniquement les requêtes GET
        return request.user \
               and request.user.is_authenticated \
               and request.method == 'GET'

class IsGuest(BasePermission):

    def __init__(self, event_id):
        self.event_id = event_id

    def has_permission(self, request, view):
        volunteer = findVolunteer(request.user, self.event_id)
        return bool(request.user
                    and request.user.is_authenticated
                    and volunteer
                    and (volunteer.permission == 'GUEST'
                         or (volunteer.role and volunteer.role.permission == 'GUEST')))


class IsMember(BasePermission):

    def __init__(self, event_id):
        self.event_id = event_id

    def has_permission(self, request, view):
        volunteer = findVolunteer(request.user, self.event_id)
        return bool(request.user
                    and request.user.is_authenticated
                    and (volunteer and (volunteer.permission == 'MEMBER'
                         or (volunteer.role and volunteer.role.permission == 'MEMBER')))
                         or IsManager(self.event_id).has_permission(request, self))


class IsManager(BasePermission):

    def __init__(self, event_id):
        self.event_id = event_id

    def has_permission(self, request, view):
        volunteer = findVolunteer(request.user, self.event_id)
        return bool(request.user
                    and request.user.is_authenticated
                    and ((volunteer and (volunteer.permission == 'MANAGER'
                         or (volunteer.role and volunteer.role.permission == 'MANAGER')))
                         or IsSuperManager(self.event_id).has_permission(request, view)))


class IsSuperManager(BasePermission):

    def __init__(self, event_id):
        self.event_id = event_id

    def has_permission(self, request, view):
        volunteer = findVolunteer(request.user, self.event_id)
        return bool(request.user
                    and request.user.is_authenticated
                    and ((volunteer and (volunteer.permission == 'SUPER_MANAGER'
                                         or (volunteer.role and volunteer.role.permission == 'SUPER_MANAGER')))
                         or IsAdmin(self.event_id).has_permission(request, view)))


class IsAdmin(BasePermission):

    def __init__(self, event_id):
        self.event_id = event_id

    def has_permission(self, request, view):
        volunteer = findVolunteer(request.user,self.event_id)
        return bool(request.user
                    and request.user.is_authenticated
                    and ((volunteer and (volunteer.permission == 'ADMIN'
                                         or (volunteer.role and volunteer.role.permission == 'ADMIN')))
                         or IsSuperAdmin().has_permission(request, view)))




class IsSuperAdmin(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user
                    and request.user.is_authenticated
                    and request.user.is_superuser)
