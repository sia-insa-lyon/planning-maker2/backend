FROM python:3.11-alpine

RUN apk add --no-cache tzdata bash && \
    cp /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    echo "Europe/Paris" >  /etc/timezone

RUN apk add --no-cache postgresql-dev gcc musl-dev

RUN apk add --no-cache gettext git

WORKDIR /app

RUN pip install --upgrade pip

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . /app

# A modfier si vous changez des paramètres
ENV DATABASE_URL postgres://postgresql:postgresql@db:5432/planningmaker2

EXPOSE 8000

CMD python /app/manage.py runserver 