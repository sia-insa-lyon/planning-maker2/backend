# PlanningMaker2

## Installation du projet

Il faut d'abord mettre en place l'environnement de développement.
```
python -m venv .venv
```

Pour activer l'environnement virtuel
```
source .venv/bin/activate
```

Ensuite, il faut installer les dépendances.
```
pip install -r requirements.txt
```

## Instruction pour le dev local

Il faut installer **Docker Desktop** pour faire tourner l'application en situation de prod, sur votre machine en local.
Avantage 1 : pas besoin d'installer postgres sur son PC pour faire marcher l'appli, car postgres s'installera tout seul dans un container.
Avantage 2 : C'est comme ça que l'appli va s'exécuter en production. Donc si ça marche sur votre machine, ça marchera en production.

Note: Il faut que ce dernier soit démarré pour que les commandes suivantes marchent.

Pour construire l'image (nécessaire à chaque modification), il faut exécuter :
```
docker-compose build
```

Ensuite, pour exécuter cette image dans un container :
```
docker-compose up
```

Les 2 commandes précédentes peuvent être réunies en 1 seule :
```
docker-compose up --build
```
