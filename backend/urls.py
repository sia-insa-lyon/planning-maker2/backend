from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from authentication import views as auth_views
from planning.views.EventView import EventViewSet
from planning.views.LocationView import LocationViewSet
from planning.views.MacroTaskView import MacroTaskViewSet
from planning.views.RoleView import RoleViewSet
from planning.views.TagView import TagViewSet
from planning.views.TaskTypeView import TaskTypeViewSet
from planning.views.TaskView import TaskViewSet
from planning.views.TimeWindowView import TimeWindowViewSet
from planning.views.TrustLevelPerMacroTaskView import TrustLevelPerMacroTaskViewSet
from planning.views.TrustLevelView import TrustLevelViewSet
from planning.views.VolunteerView import VolunteerViewSet

router = routers.SimpleRouter()
event_router = routers.SimpleRouter()
router.register('user', auth_views.UserViewSet, basename='user') # User Route
router.register('event', EventViewSet, basename='event') # Event Route
event_router.register('role', RoleViewSet, basename='role') # Role Route
event_router.register('tag', TagViewSet, basename='tag') # Tag Route
event_router.register('taskType', TaskTypeViewSet, basename='taskType') # TaskType Route
event_router.register('volunteer', VolunteerViewSet, basename='volunteer') # Volunteer Route
event_router.register('location', LocationViewSet, basename='location') # Location Route
event_router.register('task', TaskViewSet, basename='task') # Task Route
event_router.register('macroTask', MacroTaskViewSet, basename='macroTask') # MacroTask Route + Check available volunteer for this task
event_router.register('timeWindow', TimeWindowViewSet, basename='timeWindow') # TimeWindow
router.register('trustLevel', TrustLevelViewSet, basename='trustLevel')
router.register('trustLevelPerMacroTask', TrustLevelPerMacroTaskViewSet, basename='trustLevelPerMacroTask')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/signup/<int:event_id>/', auth_views.SignUp.as_view()),
    path('auth/exist/', auth_views.Exist.as_view()),
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/<int:event_id>/', auth_views.LoginEvent.as_view(), name='token_obtain_pair_event'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('event/<int:event_id>/', include(event_router.urls)),
    path('', include(router.urls)),
]
