from django.db import models

from django.contrib.auth.models import AbstractUser, PermissionsMixin

from .managers import CustomUserManager
from .validators import validate_staff, validate_superuser

class User(AbstractUser, PermissionsMixin):
    username = None
    year = models.CharField(max_length=4, null=True)
    address = models.CharField(max_length=255, null=True)
    phone = models.CharField(max_length=10, null=True)
    is_staff = models.BooleanField(default=False, validators=[validate_staff])
    is_superuser = models.BooleanField(default=False, validators=[validate_superuser])

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'year', 'address', 'phone']

    objects = CustomUserManager()


    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['email'], name='unique_mail')
        ]


    def __str__(self):
        return self.first_name + ' ' + self.last_name
