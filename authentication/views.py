import datetime

from django.contrib.auth.hashers import make_password
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import transaction
from django.http import JsonResponse
from pytz import utc
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import IsAuthenticated

from django.shortcuts import get_object_or_404

from planning.models import Volunteer, Event
from .models import User
from .serializers import UserSerializer
from planning.permissions import IsSuperAdmin


class SignUp(APIView):

    def post(self, request, event_id):
        try:
            event = Event.objects.get(id=event_id)
        except Event.DoesNotExist:
            return Response({'error': 'The specified event does not exist'}, status=status.HTTP_400_BAD_REQUEST)

        if utc.localize(datetime.datetime.now()) < event.start_registration_date or utc.localize(datetime.datetime.now()) > event.end_registration_date:
            return Response({'error': 'The registration is not open'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            validate_password(request.data['password'])
        except ValidationError:
            return Response({'error': 'The password is invalid'}, status=status.HTTP_400_BAD_REQUEST)

        request.data['password'] = make_password(request.data['password'])

        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            serializer.save()

            user = serializer.instance

            volunteer = Volunteer(event_id=event_id, user_id=user.id)
            volunteer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class Exist(APIView):

    def post(self, request):
        try:
            if User.objects.filter(email=request.data['email']).exists():
                return JsonResponse({'exist': True})
            return JsonResponse({'exist': False})
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class LoginEvent(TokenObtainPairView):
    def post(self, request, event_id, *args, **kwargs):
        # Utiliser CustomTokenObtainPairView pour générer le token d'authentification
        response = super().post(request, *args, **kwargs)

        if response.status_code == status.HTTP_200_OK:
            try:
                event = Event.objects.get(id=event_id)
            except Event.DoesNotExist:
                return Response({'error': 'The specified event does not exist'}, status=status.HTTP_400_BAD_REQUEST)

            if utc.localize(datetime.datetime.now()) < event.start_registration_date \
                    or utc.localize(datetime.datetime.now()) > event.end_registration_date:
                return Response({'error': 'The registration is not open'}, status=status.HTTP_400_BAD_REQUEST)

            email = request.data.get('email')
            user = User.objects.get(email=email)
            volunteer = Volunteer(event_id=event_id, user_id=user.id)
            volunteer.save()

        return response
    
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action in ['retrieve', 'update', 'partial_update']:
            permission_classes = [IsAuthenticated()]
        else:
            permission_classes = [IsSuperAdmin()]
        return  permission_classes


    def list(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    def create(self, request):
        request.data['password'] = make_password(request.data['password'])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, pk=None):
        if IsSuperAdmin().has_permission(request, self) or request.user.id == pk:
            user = get_object_or_404(User, id=pk)
            serializer = UserSerializer(user)
            return Response(serializer.data)
        else:
            return Response({'error': 'Method "GET" not allowed.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        if IsSuperAdmin().has_permission(request, self) or request.user.id == user.id:
            serializer = UserSerializer(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': 'Method "PUT" not allowed.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)


    def partial_update(self, request, pk=None):
        user = get_object_or_404(self.get_queryset(), id=pk)
        serializer = self.serializer_class(user, data=request.data, partial=True)
        if IsSuperAdmin().has_permission(request, self) or request.user.id == user.id:
            serializer = UserSerializer(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': 'Method "PATCH" not allowed.'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        user.delete()
        return Response({'delete completed'},status=status.HTTP_204_NO_CONTENT)
